# Redmine en AWS utilizando Docker

## Consigna

Modificar el Redmine del [Lab 2](https://gitlab.com/chudix/ansible-vagrant-redmine) de manera tal de reemplazar los servicios montados por un docker-compose.

## Plan

- [x] Utilizar lo aprendido en la [Actividad 5 del Curso de Docker](https://github.com/juanpsm/docker/blob/master/actividad-5.md) para crear un `docker-compose.yml` que permita instanciar una imagen de redmine junto con otra de mysql para establecer el servicio.

- [x] Escribir un `Dockerfile` para crear nuestra propia imagen de redmine, para servirlo con un `nginx`, y pushearla al registry de Gitlab

- [x] Utilizando Terraform, levantar una instancia de EC2 que tenga (o instale) `docker-compose` y levante los contenedores descritos anteriormente.

## Despliegue

1. Clonamos el repositorio

    ```sh
    git clone https://gitlab.com/juanpsm/docker-redmine-aws.git
    ```

1. Configuramos las variables necesarias para el despliegue en aws:

    ```sh
    export TF_VAR_pem_key_name=my_key
    export TF_VAR_pem_key_file=<path-a-la-key>
    # opcional
    export TF_VAR_redmine_db_username=redmine_user
    export TF_VAR_redmine_db_password=supersecret
    ```

1. Instalamos los providers de terraform

    ```sh
    cd terraform
    terraform init
    ```

1. Lanzamos la infraestructura

    ```sh
    terraform apply
    ```

Finalmente aceptamos, se creara la instancia y se mostrara la ip de la instancia la cual accediendo desde
el navegador nos mostrara la aplicación.

## Documentación

En lineas generales el proyecto consiste en levantar un conjunto de contenedores dentro de una instancia
EC2 en aws, que provean la aplicación **Redmine**.

A la vez se respeta la consigna del laboratorio anterior agregando backups y monitoreo.

### Docker

#### Dockerfile

Para la parte de docker del proyecto se utiliza una imagen custom de redmine, creada a partir de un multistage
build en el archivo [Dockerfile](./docker/Dockerfile). Dicha imagen se basa en la imagen oficial de redmine
y agrega un servidor de aplicación (Puma).

Esta imagen se encuentra integrada en un pipeline de gitlab, de forma tal que cada vez que se realiza un
push en el repositorio se realiza un build y se disponibiliza en el [registry del repositorio](https://gitlab.com/juanpsm/docker-redmine-aws/container_registry).

#### docker-compose.yml

Para servir la aplicación se utiliza un [docker-compose.yml](./docker/docker-compose.yml) que tiene definidos
tres servicios:

- **redmine**: es el encargado de servir la aplicación mediante Puma. Utiliza como imagen la publicada
en el registry del repositorio. Mas informacion acerca su configuracion [aca](https://hub.docker.com/_/redmine)

- **mysql**: Como su nombre lo indica corre un mysql encargado de la persistencia de datos. Utiliza como
imagen base la oficial de mysql 5.7. Mas informacion acerca su configuracion [aca](https://hub.docker.com/_/mysql)

- **nginx**: Corre dicho servidor web, quien interactua con el servicio redmine para servir la aplicación y
utiliza como imagen la oficial de nginx basada en alpine. Mas informacion acerca su configuracion [aca](https://hub.docker.com/_/nginx)

El archivo realiza interpolación de variables para mantener secretos. Por lo que, antes de ejecutar el
comando docker compose up, se deben definir dos variables de entorno:

- `REDMINE_DB_PASSWORD`
- `REDMINE_DB_USERNAME`

Se provee un ejemplo de la utilización y seteo de dichas variables en el archivo [.env.example](./docker/.env.example)
de forma tal que con solo cambiar su nombre a .env, docker-compose automáticamente se encargará de utilizarlas.

### Terraform

Para el despliegue de la aplicación en `aws` se utiliza [terraform](./terraform).

Se definen distintas variables, algunas requeridas y otras con valores por defecto:

| nombre | tipo | descripción | default |
| --- | --- | --- | --- |
| instance\_ami | string | El id de la imagen de la instancia | ami-0ba918b8809f8d365 |
| instance\_name | string | El nombre que recibirá la instancia | Redmine-instance |
| instance\_user\_name | string | El nombre de usuario por defecto para conectarse a la instancia | ubuntu |
| pem\_key\_name | string | El nombre de la key para asociar a la instancia | |
| pem\_key\_file | string | Path a la clave privada en el sistema local | |
| region | string | Region de aws para desplegar la infraestructura | us-west-2 |
| main\_az | string | Availability zone a utilizar por defecto | us-west-2a |
| redmine\_db\_username | string | Variable de ambiente para utilizar con docker-compose | redmine |
| redmine\_db\_password | string | Variable de ambiente para utilizar con docker-compose | secret |

De forma tal que sera necesario setear al menos dos variables con cualquiera de los mecanismos que
provee terraform. Por ejemplo, utilizando variables de entorno:

```bash
export TF_VAR_pem_key_name=foo
export TF_VAR_pem_key_file=/home/foo/.aws/foo.pem
```

Adicionalmente se pueden sobreescribir cualquiera de las demas variables de similar
forma, anteponiendo TF\_VAR\_<nombre-de-variable>.


La infraestructura consiste en una instancia EC2 definida en [instance.tf](./terraform/instance.tf).
Esta instancia tiene la particularidad de que utilizar una ami con docker instalado y se la aprovisiona
con los archivos de la carpeta docker(docker-compose.yml y Dockerfile) mediante el provisioner de
terraform [file](https://www.terraform.io/docs/language/resources/provisioners/file.html). El archivo
docker-compose.yml sera el que una vez que la instancia se encuentre lista se ejecutara, mediante
docker-compose, y levantara la aplicacion en el host.

La instancia EC2 tiene definidos tres volumenes EBS: 

- `mysql_data`: Utilizado para almacenar la base de datos de la aplicacion. Es un volumen de tipo
EBS, que una vez que se asigna a la instancia se formatea y se monta en el directorio `/tmp/mysql_data`.

- `redmine_volume`: Utilizado para almacenar archivos de redmine. Es un volumen EBS que una vez
que se asigna a la instancia se formatea y se monta en la carpeta `/tmp/redmine`.

- `logs_volume`: Utilizado para almacenar los logs de nginx, mysql y redmine(puma) en las carpetas
nginx, mysql y redmine respectivamente. Una vez asignado a la instancia, al igual que los anteriores
se formatea y se monta en la carpeta `/tmp/logs`.

Estos se mapean a los volúmenes declarados en el docker-compose, y como bien se establece en 
[dlm\_role.tf](./terraform/dlm_role.tf), poseen una política de backups diarios con 14 días de retención.

Una vez creada la instancia, y teniendo los volumenes disponibles, se la aprovisiona con la carpeta 
[`docker/`](./docker/), se instala el comando docker-compose y se ejecuta; levantándose así los
servicios correspondientes.

## Problemas?

Una vez finalizado el desarrollo se realizo un
[analisis de la solucion en cuanto a las buenas practicas y marcos de trabajo](./marcos-de-trabajo.md)

