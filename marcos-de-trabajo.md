# Marcos de trabajo

## Gestión de contenedores

- Utilizar mecanismos de log nativo para contenedores ❌
  > No se utiliza una herramienta para centralizar los logs.
  > Actualmente estamos montando los logs de cada servicio en directorios
  > pertenecientes a distintos discos. Una posible solución seria montar la carpeta
  > de logs de docker en un disco para tener acceso a los logs en formato json, aunque no
  > resuelve el procesamiento de los mismos.
  >
  > Lo ideal sería utilizar un cuarto contenedor que se encargue de centralizar los
  > logs de los demás y manejarlos. Por ejemplo `fluentd`.

- Asegurar que los contenedores son stateless e inmutables ⚠
   - Statelessness ⚠
     > Cumple statelessness desde el punto de vista que los archivos y la base de datos se
     > encuentran en volúmenes separados los cuales pertenecen a distintos discos. Por lo que
     > el estado de la aplicación se encuentra desacoplado del contenedor.
     >
     > No se utilizan object stores para almacenar archivos ni redis para las sesiones.

   - Inmutabilidad ⚠
     > En cuanto a la inmutabilidad, se utilizan imágenes ya configuradas a las que no hay que
     > instalarle nada más. Por lo que son desechables y reproducibles.
     >
     > En cuanto a la configuración algunas variables se encuentran parametrizadas mientras que
     > otras no. Por lo que habría que parametrizar todos las variables de configuración.
     > No hemos deshabilitado las distintas formas en las que se pueden alterar
     > los contenedores.

- Evitar contenedores privilegiados ✅
  > Efectivamente cada proceso dentro de cada contenedor corre con usuarios distintos de root
  > y con privilegios limitados.

- Permitir que las aplicaciones sean fácil de monitorear ⚠
   > La aplicación cuenta con un monitoreo de caja negra en donde se chequea que la aplicación
   > devuelva un estatus 200 en home.

   - Endpoint HTTP con métricas ❌
     > Estamos utilizando la imagen oficial de Redmine que no tiene integrada esta funcionalidad.
     > Como solución podríamos implementar un sidecar para monitoreo que se encargue de recolectar
     > y proveer las métricas.

- Exponer el estado de la aplicación ❌
   > Nuevamente, estamos utilizando una imagen oficial de Redmine que no provee endpoints para
   > chequear el estado de la aplicación. Se deberían crear estos endpoints modificando el código
   > existente.

- Evitar correr contenedores como root ❌
  > Se esta utilizando una imagen de Ubuntu con Docker instalado que no permite
  > ejecutarlo sin privilegios. Una solución podría ser crear una ami específica con `docker`
  > y `docker-compose` instalados y configurados para que puedan ser ejecutados por un usuario sin
  > privilegios.

- Elegir la versión de una imagen de forma cuidadosa ⚠
  > Para la realización del laboratorio se utilizaron imágenes oficiales cuya version se encuentra
  > especificada explícitamente.
  >
  > Sin embargo en un caso se utiliza la imágen `redmine:4.2.1-alpine` cuando hubiese sido más correcto
  > específicar la versión `redmine:4.2-alpine`

## Construcción de imágenes de contenedores

- Empaquetar una única aplicación por contenedor ✅
  > Se utilizaron 3 contenedores: uno para Redmine, otro para mysql y otro para nginx

- Manejo del PID 1, señalización de procesos y procesoszombies ❔
  > Los contenedores se encuentran corriendo cada uno un unico proceso y con PID 1.

- Optimizaciones para la caché usada por docker build ❔
  > El único Dockerfile utilizado es muy simple, por lo tanto no hay instrucciones
  > que se puedan combinar en un único RUN, por ejemplo.

- Eliminar herramientas innecesarias ❌
  > Si bien se utilizan como base imagenes de alpine, no se tomaron medidas
  > para minimizar totalmente las herramientas instaladas.

- Construir imágenes lo más pequeñas posible ✅
  > Como se menciona arriba se intenta partir de imágenes basadas en `alpine`
  > siempre que es posible, salvo en el caso de mysql que no tiene esta opción
  > de forma oficial. Igualmente su imágen se construya a partir de
  > `oisupport/bashbrew:base` que a su vez parte de `golang:1.16-buster`.

- Utilizar usuarios con el menor privilegio posible ✅
  > Actualmente los usuarios con los que corren los procesos en los contenedores
  > no tienen privilegios mas que para ejecutar su tarea.

- Firmar y verificar imágenes ❌
  > No se valida la integridad de las imágenes antes de su descarga ni se firman
  > las creadas.

- Evitar la fuga de información sensible en imágenes ⚠
  > Se utiliza un archivo `.env` como alternativa a
  > setear unas variables de entorno. Este archivo se agregó al `.gitignore`
  > pero no se creó un `.dockerignore` asiq ue cuando se construyen imagenes
  > y se pasa el contexto de la carpeta `docker/` este archivo y las credenciales
  > que se hayan escrito en el quedarán en la historia.
  > Deberíamos implementar los `secrets` del BuildKit.

- Usar COPY en vez de ADD ✅
  > En las imágenes utilizadas siempre se utiliza la instrución COPY. En las
  > que hemos construido no tuvimos la necesidad de realizarlo, dado que se
  > montan directamente los volúmenes.

- Escaneo de vulnerabilidades  en imágenes ❌
  > No hemos realizado esta operación. Al momento de intentarlo:
  > ```
  > docker scan --file Dockerfile registry.gitlab.com/juanpsm/docker-redmine-aws
  > manifest unknown
  > ```

- Tag de imágenes apropiado ✅
  > Para redmine se utliza un build del dockerfile del repositorio, que se guarda en
  > el registry del mismo. Las imagenes se taguean con el slug del commit de git: `$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG`.
  > De todas formas, este tag no se especifica en el docker-compose.yml por lo que siempre
  > se utilizara la ultima imagen subida al branch main. Perdiendose asi el control de
  > la imagen utilizada.
    
- Usar un linter ❌
- Consideraciones al utilizar imágenes públicas ❔
  > Para la realizacion del laboratorio se utilizan imagenes oficiales y publicas, por
  > lo que muchas consideraciones de seguridad, implementaciones y configuraciones
  > son confiadas a terceros. Lo ideal seria construir nuestras propias imagenes, o bien
  > realizar una auditoria sobre las imagenes utilizadas.

