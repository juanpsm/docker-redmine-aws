# Laboratorio 3 - Redmine usando docker

Este laboratorio consiste en levantar una instancia EC2 en amazon
que proveera la aplicacion Redmine mediante contenedores de docker.

Adicionalmente se provee la logica para realizar backups de aquellos
datos que consideramos vale la pena tener resguardados.

## Docker

## Terraform

A grandes rasgos utilizamos terraform para crear una instancia y 
levantar los contenedores necesarios para correr la aplicacion.

Para poder ejecutar el terraform apply sera necesario definir
ciertas variables que se encuentran especificadas en [variables.tf](./terraform/variables.tf)
(aquellas que no tienen un valor por defecto):

| nombre | tipo | descripción | default |
| --- | --- | --- | --- |
| instance\_ami | string | El id de la imagen de la instancia | ami-0ba918b8809f8d365 |
| instance\_name | string | El nombre que recibirá la instancia | Redmine-instance |
| instance\_user\_name | string | El nombre de usuario por defecto para conectarse a la instancia | ubuntu |
| pem\_key\_name | string | El nombre de la key para asociar a la instancia | |
| pem\_key\_file | string | Path a la clave privada en el sistema local | |
| region | string | Region de aws para desplegar la infraestructura | us-west-2 |
| main\_az | string | Availability zone a utilizar por defecto | us-west-2a |
| redmine\_db\_username | string | Variable de ambiente para utilizar con docker-compose | redmine |
| redmine\_db\_password | string | Variable de ambiente para utilizar con docker-compose | secret |

El entrypoint de la infraestructura es el archivo [main.tf](./terraform/main.tf) donde
se especifica la region donde se va a trabajar.

Luego tenemos el archivo [instances.tf](./terraform/instances.tf) donde se configura la instancia
y se aprovisiona con los archivos de la carpeta [docker](./docker). Aca tenemos el 
docker-compose que se levanta una vez creada la instancia.

En el archivo [security_groups](./terraform/security_groups.tf) es donde configuramos los
correspondientes security groups para comunicarnos con la instancia (ssh y http).

En el archivo [volumes.tf](./terraform/volumes) definimos los volumenes con los
que trabajara nuestra instancia. Para el desarrollo de este laboratorio se
utilizan 3 volumenes:

1. Logs generales

2. Archivos de redmine

3. Base de datos.

Estos volumenes se crean y montan en directorios especificos que son utilizados
por docker-compose para mapear a los volumenes de los contenedores. De esta forma
tenemos los datos separados de los contenedores de forma tal que se puedan realizar
backups separados y restauraciones separadas.

Una vez que estos volumenes se han creado, podemos levantar la aplicacion mediante
docker-compose.

En el archivo [dlm\_role](./terraform/dlm_role.tf) definimos las politicas de backup
que especifican que se realiza un backup de cada volumen cada 24 horas con una 
retencion de 2 semanas.
 
