#create first EBS Volume for mysql data
resource "aws_ebs_volume" "mysql_data" {
  availability_zone = var.main_az
  type              = "gp2"
  size              = 10
  tags = {
    Name = "mysql_data"
  }
}

resource "aws_volume_attachment" "mysql_data_attach" {
  device_name  = "/dev/sdh"
  volume_id    = aws_ebs_volume.mysql_data.id
  instance_id  = aws_instance.app_server.id
  force_detach = true
}

#Mount EBS and Add data files.
resource "null_resource" "mysql_mount" {
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${var.pem_key_file}")
    host        = aws_instance.app_server.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mkfs -t ext4 /dev/xvdh",
      "sudo mkdir /tmp/mysql_data",
      "sudo mount /dev/xvdh /tmp/mysql_data",
      "sudo mkdir /tmp/mysql_data/data",
    ]

  }
  depends_on = [aws_volume_attachment.mysql_data_attach]
}
##### create second volume for redmine
resource "aws_ebs_volume" "redmine_volume" {
  availability_zone = var.main_az
  type              = "gp2"
  size              = 5
  tags = {
    Name = "redmine_volume"
  }
}

resource "aws_volume_attachment" "redmine_volume_attach" {
  device_name  = "/dev/sdi"
  volume_id    = aws_ebs_volume.redmine_volume.id
  instance_id  = aws_instance.app_server.id
  force_detach = true
}

#Mount EBS and Add data files.
resource "null_resource" "redmine_mount" {
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${var.pem_key_file}")
    host        = aws_instance.app_server.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mkfs -t ext4 /dev/xvdi",
      "sudo mkdir /tmp/redmine",
      "sudo mount /dev/xvdi /tmp/redmine",
      "sudo mkdir /tmp/redmine/data /tmp/redmine/plugins",
      "sudo chown -R ubuntu:ubuntu /tmp/redmine"
    ]

  }
  depends_on = [aws_volume_attachment.redmine_volume_attach]
}

##### create third volume for logs
resource "aws_ebs_volume" "logs_volume" {
  availability_zone = var.main_az
  type              = "gp2"
  size              = 2
  tags = {
    Name = "logs_volume"
  }
}

resource "aws_volume_attachment" "logs_volume_attach" {
  device_name  = "/dev/sdj"
  volume_id    = aws_ebs_volume.logs_volume.id
  instance_id  = aws_instance.app_server.id
  force_detach = true
}

#Mount EBS and Add data files.
resource "null_resource" "logs_mount" {
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${var.pem_key_file}")
    host        = aws_instance.app_server.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mkfs -t ext4 /dev/xvdj",
      "sudo mkdir /tmp/logs",
      "sudo mount /dev/xvdj /tmp/logs",
      "sudo mkdir /tmp/logs/redmine /tmp/logs/mysql /tmp/logs/nginx",
      "sudo chown 999:ubuntu -R /tmp/logs/redmine"
    ]

  }
  depends_on = [aws_volume_attachment.logs_volume_attach]
}

resource "null_resource" "start_containers" {
  provisioner "remote-exec" {
    inline = [
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo REDMINE_DB_PASSWORD=${var.redmine_db_password} REDMINE_DB_USERNAME=${var.redmine_db_username} docker-compose -f /tmp/docker/docker-compose.yml up -d"
    ]
    #
    connection {
      host        = aws_instance.app_server.public_ip
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("${var.pem_key_file}")
    }

  }
  depends_on = [null_resource.mysql_mount,
  null_resource.redmine_mount]
}
