output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

output "app_address" {
  description = "Public IP address of the EC2 instance"
  value       = "http://${aws_instance.app_server.public_ip}"
}

output "puma_address" {
  description = ""
  value       = "http://${aws_instance.app_server.public_ip}:3000"
}

output "phpMyAdmin_address" {
  description = ""
  value       = "http://${aws_instance.app_server.public_ip}:8080"
}