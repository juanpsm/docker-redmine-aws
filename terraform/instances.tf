resource "aws_instance" "app_server" {
  ami               = var.instance_ami
  instance_type     = "t2.micro"
  availability_zone = var.main_az
  key_name          = var.pem_key_name
  security_groups = [
    aws_security_group.allow_http.name,
    aws_security_group.allow_ssh.name,
    aws_security_group.allow_all_egress.name
  ]

  provisioner "file" {
    source      = "../docker"
    destination = "/tmp/docker"

    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("${var.pem_key_file}")
    }
  }



  tags = {
    Name = var.instance_name
  }
}
