# Configure the UptimeRobot Provider
provider "uptimerobot" {
  # api_key = "xxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxx"
  # Specified using environment variable: UPTIMEROBOT_API_KEY
}

# This api does not support Discord yet, so create the contact through the
# webapp and then TF can look it up by friendly-name
data "uptimerobot_alert_contact" "discord_contact" {
  friendly_name = "discord-test"
}

# To set up the default contact to the account email:
# data "uptimerobot_account" "account" {}
# data "uptimerobot_alert_contact" "default_alert_contact" {
#   friendly_name = data.uptimerobot_account.account.email
# }

# Custom contact by email
# resource "uptimerobot_alert_contact" "juanp" {
#   friendly_name = "Juanp"
#   type          = "email"
#   value         = "juan.sanchez@mikroways.com"
# }

# Create a monitor
resource "uptimerobot_monitor" "main" {
  friendly_name = "Redmine-${aws_instance.app_server.public_ip}"
  type          = "http"
  url           = "http://${aws_instance.app_server.public_ip}"

  alert_contact {
    id = data.uptimerobot_alert_contact.discord_contact.id
  }

  # alert_contact {
  #   id = data.uptimerobot_alert_contact.default_alert_contact.id
  # }
}
